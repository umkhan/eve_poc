from eve import Eve
from eve_sqlalchemy import SQL
from eve_sqlalchemy.validation import ValidatorSQL
from eve_sqlalchemy.config import DomainConfig, ResourceConfig
from flask_swagger_ui import get_swaggerui_blueprint
from eve_swagger import swagger, add_documentation
from model import populate_db_if_empty, Participant, Plan, Loan, Enrollment, LoanReqHistory, Base
from settings import (SWAGGER_URL,
                      API_URL,
                      APP_HOST,
                      APP_PORT,
                      SWAGGER_INFO,
                      DEBUG,
                      SQLALCHEMY_DATABASE_URI, SUPPORTED_SUBMIT_METHODS)

DOMAIN = DomainConfig({
        'participant': ResourceConfig(Participant),
        'plan': ResourceConfig(Plan),
        'loan': ResourceConfig(Loan),
        'enrollment': ResourceConfig(Enrollment),
        'loanreqhistory': ResourceConfig(LoanReqHistory)
        }).render()

SETTINGS = {
    'DEBUG': DEBUG,
    'SQLALCHEMY_DATABASE_URI': SQLALCHEMY_DATABASE_URI,
    'DOMAIN': DOMAIN,
    'SQLALCHEMY_TRACK_MODIFICATIONS': True,
    'RESOURCE_METHODS': ['POST', 'GET', 'DELETE'],
    'ITEM_METHODS': ['GET','PATCH', 'PUT','DELETE'],
    # 'OPLOG' : True,
    # 'OPLOG_ENDPOINT':'oplogger'
}


def log_every_get(resource, request, payload):
    app.logger.info('GET request!')


app = Eve(auth=None, settings=SETTINGS, validator=ValidatorSQL, data=SQL)
app.on_post_GET += log_every_get

# To stop projection of firstname fieild in get calls
DOMAIN['loan']['datasource']['projection']['firstname'] = 0


# To enable embedding
DOMAIN['loan']['schema']['enrollment']['data_relation']['embeddable'] = True
DOMAIN['loan']['embedded_fields'] = ['enrollment']

DOMAIN['loan'].update({
    #'item_title': 'person',
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,
    'resource_methods': ['GET']
})
# DOMAIN['oplog']  = { 'url': 'oplogger', 'datasource': {'source': 'oplog'} }

# required. See http://swagger.io/specification/#infoObject for details.
app.config['SWAGGER_INFO'] = SWAGGER_INFO

# bind SQLAlchemy
db = app.data.driver
Base.metadata.bind = db.engine
db.Model = Base
db.create_all()

# Insert some example data in the db

populate_db_if_empty(db)

# Call factory function to create our blueprint
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'supportedSubmitMethods': SUPPORTED_SUBMIT_METHODS
    }
)

from groot_shared import shared_blueprint

app.register_blueprint(shared_blueprint, url_prefix='/shared')

# Register blueprint at URL
# (URL must match the one given to factory function above)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)
app.register_blueprint(swagger)
if __name__ == '__main__':
    app.run(host=APP_HOST,port=int(APP_PORT),debug=True, use_reloader=False)