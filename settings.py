import os
DEBUG = True

SWAGGER_INFO={
    'title': 'EVE DEMO',
    'version': '0.01 beta',
    'description': 'This demo is review EVE and its extension SQL Alchemy',
    'termsOfService': '',
    'contact': {
        'name': 'Umair Khan',
        'url': 'http://wwww.xyz.com'
    },
    'license': {
        'name': 'BSD',
        'url': 'http://wwww.xyz.com'
    }
}

# Initialize Objects
# capture current working directory
PWD = os.getenv("PWD")
# set default host and ports (change 5000 to avoid airplay collision)
APP_HOST = '0.0.0.0'
APP_PORT = os.getenv('PORT', '5005')
APP_URI = 'http://0.0.0.0:5005'
SQLALCHEMY_DATABASE_URI = 'postgresql://%s:%s/%s' % ('localhost', 5432, 'postgres')

# Enable URL_PREFIX.  Used in conjunction with API_VERSION to build
# API Endpoints of the form <base_route>/<url_prefix>/<api_version>/
URL_PREFIX = 'api'

# Enable API Versioning.  This will force API Calls to follow a form of
# <base_route>/<api_version>/<resource_title>/...
API_VERSION = 'v1'

# Enable reads (GET), inserts (POST) and DELETE for resources/collections
# (if you omit this line, the API will default to ['GET'] and provide
# read-only access to the endpoint).
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']

# Enable reads (GET), edits (PATCH) and deletes of individual items
# (defaults to read-only item access).
ITEM_METHODS = ['GET', 'PATCH', 'DELETE']

DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'

# We enable standard client cache directives for all resources exposed by the
# API. We can always override these global settings later.
CACHE_CONTROL = 'max-age=20'
CACHE_EXPIRES = 20
SUPPORTED_SUBMIT_METHODS = ['get', 'put', 'post', 'delete']
# Accept-Language request headers
LANGUAGE_DEFAULT = 'en'
LANGUAGES = {
    'en': 'English',
    'es': 'Espanol',
    'fr': 'French',
    'pt': 'Portuguese',
    'ar': 'Arabic'
}

# We enable Cross Origin Resource Sharing (CORS) to facilitate swagger.io
X_DOMAINS = '*'
X_HEADERS = ['Origin', 'X-Requested-With', 'Content-Type', 'Accept']


SWAGGER_URL = '/api-docs-ui'  # URL for exposing Swagger UI (without trailing '/')
API_URL = '/api-docs' # Our API url (can of course be a local resource)
