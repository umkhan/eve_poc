# EVE POC

For evaluation of EVE features and its compatibility with application requirements.

Reference Material
-

https://github.com/bobquest33/python_eve_demo/blob/master/loans_rest_server.py
https://github.com/pyeve/eve-demo
https://github.com/goneri/trollhunter/blob/master/wsgi.py
https://medium.com/gopypi/100-scripts-in-30-days-challenge-script-28-python-eve-for-restful-endpoints-for-databases-also-a93e9c596896
http://docs.python-eve.org/en/latest/features.html#enhanced-logging


Sample links for get_all
-
http://0.0.0.0:5005/loanreqhistory?where=_id>2 and _id<4
http://0.0.0.0:5005/participant/1/?projection={"enrollment":1}
http://0.0.0.0:5005/loanreqhistory?where={"status":"Failed"}
http://0.0.0.0:5005/enrollment?embedded={"participant":1}